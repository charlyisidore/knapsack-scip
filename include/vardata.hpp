/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef VARDATA_HPP
#define VARDATA_HPP

#include <objscip/objvardata.h>

/**
 * Knapsack variable data.
 */
class Vardata : public scip::ObjVardata
{
public:
    /**
     * Constructor.
     */
    Vardata(SCIP_Longint p, SCIP_Longint w)
        : _profit(p), _weight(w) {}

    /**
     * Returns the profit of the item.
     */
    auto profit() const
    {
        return _profit;
    }

    /**
     * Returns the weight of the item.
     */
    auto weight() const
    {
        return _weight;
    }

    /**
     * Returns the data associated to a variable.
     */
    static Vardata *get(SCIP *scip, SCIP_VAR *var)
    {
        return static_cast<Vardata *>(SCIPgetObjVardata(scip, var));
    }

private:
    SCIP_Longint _profit = 0;
    SCIP_Longint _weight = 0;
};

#endif