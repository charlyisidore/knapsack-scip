/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PROBDATA_HPP
#define PROBDATA_HPP

#include <objscip/objprobdata.h>
#include <vector>

/**
 * Knapsack problem data.
 */
class Probdata : public scip::ObjProbData
{
public:
    /**
     * Default constructor.
     */
    Probdata(SCIP_Longint capacity);

    /**
     * Copy constructor.
     */
    Probdata(const Probdata &other)
        : _capacity(other._capacity), _vars(other._vars) {}

    /**
     * Returns the capacity of the knapsack.
     */
    auto capacity() const
    {
        return _capacity;
    }

    /**
     * Returns the ordered list of variables.
     */
    const auto &vars() const
    {
        return _vars;
    }

    /**
     * Creates the problem.
     */
    [[nodiscard]] static SCIP_RETCODE create_prob(SCIP *scip,
                                                  const char *name,
                                                  const std::vector<SCIP_Longint> &profits,
                                                  const std::vector<SCIP_Longint> &weights,
                                                  SCIP_Longint capacity);

    /**
     * Returns the problem data.
     */
    static Probdata *get(SCIP *scip)
    {
        return static_cast<Probdata *>(SCIPgetObjProbData(scip));
    }

    /**
     * Destructor of user problem data to free original user data (called when
     * original problem is freed).
     */
    SCIP_RETCODE scip_delorig(SCIP *scip) override;

    /**
     * Creates user data of transformed problem by transforming the original
     * user problem data (called after problem was transformed).
     */
    SCIP_RETCODE scip_trans(SCIP *scip,
                            scip::ObjProbData **objprobdata,
                            SCIP_Bool *deleteobject) override;

    /**
     * Destructor of user problem data to free transformed user data (called
     * when transformed problem is freed).
     */
    SCIP_RETCODE scip_deltrans(SCIP *scip) override;

private:
    /**
     * Initializes the model.
     */
    [[nodiscard]] SCIP_RETCODE _initialize(SCIP *scip,
                                           const std::vector<SCIP_Longint> &profits,
                                           const std::vector<SCIP_Longint> &weights);

    /**
     * Releases the model.
     */
    [[nodiscard]] SCIP_RETCODE _release(SCIP *scip);

    SCIP_Longint _capacity = 0;
    std::vector<SCIP_VAR *> _vars;
};

#endif