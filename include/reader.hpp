/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef READER_HPP
#define READER_HPP

#include <objscip/objreader.h>

/**
 * Reader for the knapsack problem.
 */
class Reader : public scip::ObjReader
{
public:
    /**
     * File reader properties.
     */
    struct Properties
    {
        /**
         * Name of file reader.
         */
        static const char *Name;

        /**
         * Description of file reader.
         */
        static const char *Desc;

        /**
         * File extension that reader processes.
         */
        static const char *Extension;
    };

    /**
     * Default constructor.
     */
    Reader(SCIP *scip);

    /**
     * Problem reading method of reader.
     */
    SCIP_RETCODE scip_read(SCIP *scip,
                           SCIP_READER *reader,
                           const char *filename,
                           SCIP_RESULT *result) override;
};

#endif