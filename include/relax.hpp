/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef RELAX_HPP
#define RELAX_HPP

#include <objscip/objrelax.h>

/**
 * Relaxator for the knapsack problem.
 */
class Relax : public scip::ObjRelax
{
public:
    /**
     * Relaxator properties.
     */
    struct Properties
    {
        /**
         * Name of relaxator.
         */
        static const char *Name;

        /**
         * Description of relaxator.
         */
        static const char *Desc;

        /**
         * Priority of the relaxator.
         */
        static const int Priority;

        /**
         * Frequency for calling relaxator.
         */
        static const int Frequency;

        /**
         * Does the relaxator contain all cuts in the LP?
         */
        static const SCIP_Bool Includeslp;
    };

    /**
     * Default constructor.
     */
    Relax(SCIP *scip);

    /**
     * Execution method of relaxator.
     */
    SCIP_RETCODE scip_exec(SCIP *scip,
                           SCIP_RELAX *relax,
                           SCIP_Real *lowerbound,
                           SCIP_RESULT *result) override;
};

#endif