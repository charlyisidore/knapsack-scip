cmake_minimum_required(VERSION 3.12)
project(knapsack)

find_package(SCIP REQUIRED)

if (MSVC)
    add_compile_options(/W4)
else()
    add_compile_options(-Wall -Wextra -Wfatal-errors -pedantic)
endif()

add_library(${PROJECT_NAME}_lib STATIC
            src/conshdlr.cpp
            src/heur.cpp
            src/probdata.cpp
            src/reader.cpp
            src/relax.cpp)
set_target_properties(${PROJECT_NAME}_lib PROPERTIES OUTPUT_NAME ${PROJECT_NAME})
target_compile_features(${PROJECT_NAME}_lib PUBLIC cxx_std_17)
target_compile_options(${PROJECT_NAME}_lib PUBLIC $<$<CONFIG:Debug>:-DSCIP_DEBUG>)
target_include_directories(${PROJECT_NAME}_lib PUBLIC include ${SCIP_INCLUDE_DIRS})
target_link_libraries(${PROJECT_NAME}_lib PUBLIC ${SCIP_LIBRARIES})

add_executable(${PROJECT_NAME}_exe src/main.cpp)
set_target_properties(${PROJECT_NAME}_exe PROPERTIES OUTPUT_NAME ${PROJECT_NAME})
target_link_libraries(${PROJECT_NAME}_exe PUBLIC ${PROJECT_NAME}_lib)