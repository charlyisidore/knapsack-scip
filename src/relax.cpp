/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "relax.hpp"
#include "probdata.hpp"
#include "vardata.hpp"
#include <cassert>
#include <scip/cons_knapsack.h>
#include <vector>

const char *Relax::Properties::Name = "knapsack";
const char *Relax::Properties::Desc = "Relaxator for the knapsack problem";
const int Relax::Properties::Priority = 10;
const int Relax::Properties::Frequency = 1;
const SCIP_Bool Relax::Properties::Includeslp = true;

Relax::Relax(SCIP *scip)
    : scip::ObjRelax(scip,
                     Properties::Name,
                     Properties::Desc,
                     Properties::Priority,
                     Properties::Frequency,
                     Properties::Includeslp)
{
}

SCIP_RETCODE Relax::scip_exec(SCIP *scip,
                              SCIP_RELAX *relax,
                              SCIP_Real *lowerbound,
                              SCIP_RESULT *result)
{
    assert(lowerbound != nullptr);
    assert(result != nullptr);

    *lowerbound = -SCIPinfinity(scip);
    *result = SCIP_DIDNOTRUN;

    const auto *probdata = Probdata::get(scip);

    assert(probdata != nullptr);

    const auto &vars = probdata->vars();
    auto rc = probdata->capacity();

    // Selected items
    std::vector<SCIP_VAR *> s;

    // Cumulative profit of selected items
    SCIP_Longint profit = 0;

    // Critical item (first item that cannot be selected)
    SCIP_VAR *k_var = nullptr;

    // Loop over variables fixed to 1
    for (auto var : vars)
    {
        // Is var not fixed to 1?
        if (!SCIPisGT(scip, SCIPvarGetLbLocal(var), 0.5))
        {
            continue;
        }

        auto *vardata = Vardata::get(scip, var);

        assert(vardata != nullptr);

        auto w = vardata->weight();

        if (rc < w)
        {
            SCIPdebugMsg(scip, "cutting off node\n");
            *result = SCIP_CUTOFF;
            return SCIP_OKAY;
        }

        s.push_back(var);
        profit += vardata->profit();
        rc -= w;
    }

    // Loop over unfixed variables
    for (auto var : vars)
    {
        // Is var fixed?
        if (SCIPisLT(scip, SCIPvarGetUbLocal(var), 0.5) ||
            SCIPisGT(scip, SCIPvarGetLbLocal(var), 0.5))
        {
            continue;
        }

        auto *vardata = Vardata::get(scip, var);

        assert(vardata != nullptr);

        auto w = vardata->weight();

        // Can the item be selected?
        if (rc < w)
        {
            // Save the critical item and stop
            k_var = var;
            break;
        }

        // Select item
        s.push_back(var);
        profit += vardata->profit();
        rc -= w;
    }

    assert(rc >= 0);

    // Value for the critical item
    SCIP_Real r = 0.;

    // Relaxation value (for minimization)
    SCIP_Real obj = -SCIP_Real(profit);

    // Add the fractional value of the critical item
    if (k_var != nullptr)
    {
        auto *vardata = Vardata::get(scip, k_var);

        assert(vardata != nullptr);

        r = SCIP_Real(rc) / SCIP_Real(vardata->weight());
        obj -= SCIP_Real(vardata->profit()) * r;
    }

    // Does it improve the best relaxation solution?
    if (!SCIPisRelaxSolValid(scip) || SCIPisGT(scip, obj, SCIPgetRelaxSolObj(scip)))
    {
        SCIPdebugMsg(scip, "improves best relaxation\n");

        // Set all variables to zero
        SCIP_CALL(SCIPclearRelaxSolVals(scip, relax));

        for (auto var : s)
        {
            SCIP_CALL(SCIPsetRelaxSolVal(scip, relax, var, 1.));
        }

        if (k_var != nullptr)
        {
            SCIP_CALL(SCIPsetRelaxSolVal(scip, relax, k_var, r));
        }

        // Inform SCIP that the relaxation can be enforced through linear cuts
        SCIP_CALL(SCIPmarkRelaxSolValid(scip, relax, true));
    }

    SCIPdebugMsg(scip, "relax obj = %g\n", -obj);

    *lowerbound = obj;
    *result = SCIP_SUCCESS;

    return SCIP_OKAY;
}