/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "conshdlr.hpp"
#include "heur.hpp"
#include "reader.hpp"
#include "relax.hpp"
#include <cstdlib>
#include <objscip/objscip.h>
#include <scip/branch_mostinf.h>
#include <scip/cons_integral.h>
#include <scip/dialog_default.h>
#include <scip/disp_default.h>
#include <scip/heur_actconsdiving.h>
#include <scip/heur_coefdiving.h>
#include <scip/heur_crossover.h>
#include <scip/heur_dins.h>
#include <scip/heur_feaspump.h>
#include <scip/heur_fixandinfer.h>
#include <scip/heur_fracdiving.h>
#include <scip/heur_guideddiving.h>
#include <scip/heur_intdiving.h>
#include <scip/heur_intshifting.h>
#include <scip/heur_linesearchdiving.h>
#include <scip/heur_localbranching.h>
#include <scip/heur_mutation.h>
#include <scip/heur_objpscostdiving.h>
#include <scip/heur_octane.h>
#include <scip/heur_oneopt.h>
#include <scip/heur_pscostdiving.h>
#include <scip/heur_rens.h>
#include <scip/heur_rins.h>
#include <scip/heur_rootsoldiving.h>
#include <scip/heur_rounding.h>
#include <scip/heur_shifting.h>
#include <scip/heur_simplerounding.h>
#include <scip/heur_trivial.h>
#include <scip/heur_trysol.h>
#include <scip/heur_twoopt.h>
#include <scip/heur_undercover.h>
#include <scip/heur_veclendiving.h>
#include <scip/heur_zirounding.h>
#include <scip/nodesel_bfs.h>
#include <scip/scip.h>
#include <scip/scipshell.h>
#include <scip/table_default.h>

// #define USE_HEURISTICS

[[nodiscard]] static SCIP_RETCODE include_plugins(SCIP *scip)
{
    SCIP_CALL(SCIPincludeBranchruleMostinf(scip));
    SCIP_CALL(SCIPincludeConshdlrIntegral(scip));
    SCIP_CALL(SCIPincludeNodeselBfs(scip));
    SCIP_CALL(SCIPincludeTableDefault(scip));

    SCIP_CALL(SCIPincludeObjConshdlr(scip, new Conshdlr(scip), true));
    SCIP_CALL(SCIPincludeObjHeur(scip, new Heur(scip), true));
    SCIP_CALL(SCIPincludeObjReader(scip, new Reader(scip), true));
    SCIP_CALL(SCIPincludeObjRelax(scip, new Relax(scip), true));

#ifdef USE_HEURISTICS
    SCIP_CALL(SCIPincludeHeurActconsdiving(scip));
    SCIP_CALL(SCIPincludeHeurCoefdiving(scip));
    SCIP_CALL(SCIPincludeHeurCrossover(scip));
    SCIP_CALL(SCIPincludeHeurDins(scip));
    SCIP_CALL(SCIPincludeHeurFixandinfer(scip));
    SCIP_CALL(SCIPincludeHeurFracdiving(scip));
    SCIP_CALL(SCIPincludeHeurGuideddiving(scip));
    SCIP_CALL(SCIPincludeHeurIntdiving(scip));
    SCIP_CALL(SCIPincludeHeurIntshifting(scip));
    SCIP_CALL(SCIPincludeHeurLinesearchdiving(scip));
    SCIP_CALL(SCIPincludeHeurLocalbranching(scip));
    SCIP_CALL(SCIPincludeHeurMutation(scip));
    SCIP_CALL(SCIPincludeHeurObjpscostdiving(scip));
    SCIP_CALL(SCIPincludeHeurOctane(scip));
    SCIP_CALL(SCIPincludeHeurOneopt(scip));
    SCIP_CALL(SCIPincludeHeurPscostdiving(scip));
    SCIP_CALL(SCIPincludeHeurRens(scip));
    SCIP_CALL(SCIPincludeHeurRins(scip));
    SCIP_CALL(SCIPincludeHeurRootsoldiving(scip));
    SCIP_CALL(SCIPincludeHeurRounding(scip));
    SCIP_CALL(SCIPincludeHeurShifting(scip));
    SCIP_CALL(SCIPincludeHeurSimplerounding(scip));
    SCIP_CALL(SCIPincludeHeurTrivial(scip));
    SCIP_CALL(SCIPincludeHeurTrySol(scip));
    SCIP_CALL(SCIPincludeHeurTwoopt(scip));
    SCIP_CALL(SCIPincludeHeurUndercover(scip));
    SCIP_CALL(SCIPincludeHeurVeclendiving(scip));
    SCIP_CALL(SCIPincludeHeurZirounding(scip));
#endif

    SCIP_CALL(SCIPincludeDispDefault(scip));
    SCIP_CALL(SCIPincludeDialogDefaultBasic(scip));

    return SCIP_OKAY;
}

static SCIP_RETCODE run(int argc, char **argv)
{
    SCIP *scip;

    SCIP_CALL(SCIPcreate(&scip));

    SCIPenableDebugSol(scip);

    SCIP_CALL(include_plugins(scip));

    SCIP_CALL(SCIPsetBoolParam(scip, "lp/presolving", false));
    SCIP_CALL(SCIPsetIntParam(scip, "lp/solvefreq", -1));
    SCIP_CALL(SCIPsetIntParam(scip, "presolving/maxrounds", 0));
    SCIP_CALL(SCIPsetSeparating(scip, SCIP_PARAMSETTING_OFF, true));

    SCIP_CALL(SCIPprocessShellArguments(scip, argc, argv, nullptr));

    SCIP_CALL(SCIPfree(&scip));

    BMScheckEmptyMemory();

    return SCIP_OKAY;
}

int main(int argc, char **argv)
{
    SCIP_RETCODE retcode = run(argc, argv);

    if (retcode != SCIP_OKAY)
    {
        SCIPprintError(retcode);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}