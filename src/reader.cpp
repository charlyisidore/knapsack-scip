/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "reader.hpp"
#include "probdata.hpp"
#include <cassert>
#include <filesystem>
#include <fstream>
#include <vector>

const char *Reader::Properties::Name = "knapsack";
const char *Reader::Properties::Desc = "File reader for the knapsack problem";
const char *Reader::Properties::Extension = "dat";

Reader::Reader(SCIP *scip)
    : scip::ObjReader(scip,
                      Properties::Name,
                      Properties::Desc,
                      Properties::Extension)
{
}

SCIP_RETCODE Reader::scip_read(SCIP *scip,
                               [[maybe_unused]] SCIP_READER *reader,
                               const char *filename,
                               SCIP_RESULT *result)
{
    assert(result != nullptr);

    std::size_t n_items;
    std::vector<SCIP_Longint> profits;
    std::vector<SCIP_Longint> weights;
    SCIP_Longint capacity;
    std::ifstream file(filename);

    if (!file.is_open())
    {
        SCIPerrorMessage("cannot open file <%s>\n", filename);
        return SCIP_READERROR;
    }

    file >> n_items;

    profits.resize(n_items);
    weights.resize(n_items);

    for (std::size_t i = 0; i < n_items; ++i)
    {
        SCIP_Longint p;
        file >> p;
        profits[i] = p;
    }

    for (std::size_t i = 0; i < n_items; ++i)
    {
        SCIP_Longint w;
        file >> w;
        weights[i] = w;
    }

    file >> capacity;

    SCIP_CALL(Probdata::create_prob(scip,
                                    std::filesystem::path(filename).filename().c_str(),
                                    profits,
                                    weights,
                                    capacity));

    *result = SCIP_SUCCESS;

    return SCIP_OKAY;
}