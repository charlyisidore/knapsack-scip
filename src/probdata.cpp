/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "probdata.hpp"
#include "vardata.hpp"
#include <algorithm>
#include <cassert>
#include <sstream>

Probdata::Probdata(SCIP_Longint capacity)
    : _capacity(capacity)
{
}

SCIP_RETCODE Probdata::create_prob(SCIP *scip,
                                   const char *name,
                                   const std::vector<SCIP_Longint> &profits,
                                   const std::vector<SCIP_Longint> &weights,
                                   SCIP_Longint capacity)
{
    auto *probdata = new Probdata(capacity);

    SCIP_CALL(SCIPcreateObjProb(scip, name, probdata, true));

    SCIP_CALL(probdata->_initialize(scip, profits, weights));

    return SCIP_OKAY;
}

SCIP_RETCODE Probdata::_initialize(SCIP *scip,
                                   const std::vector<SCIP_Longint> &profits,
                                   const std::vector<SCIP_Longint> &weights)
{
    assert(std::size(profits) == std::size(weights));

    std::size_t n_items = std::size(profits);

    SCIP_CALL(SCIPsetObjsense(scip, SCIP_OBJSENSE_MAXIMIZE));
    SCIP_CALL(SCIPsetObjIntegral(scip));

    _vars.resize(n_items);

    for (std::size_t i = 0; i < n_items; ++i)
    {
        SCIP_VAR *var;
        std::ostringstream oss;

        oss << "x{" << i + 1 << "}";

        SCIP_CALL(SCIPcreateObjVar(scip,
                                   &var,
                                   oss.str().c_str(),
                                   0.,
                                   1.,
                                   profits[i],
                                   SCIP_VARTYPE_BINARY,
                                   true, // initial
                                   true, // removable
                                   new Vardata(profits[i], weights[i]),
                                   true));

        _vars[i] = var;

        SCIP_CALL(SCIPaddVar(scip, var));
    }

    // Sort variables by decreasing ratio profit/weight
    std::stable_sort(std::begin(_vars),
                     std::end(_vars),
                     [scip](auto var1, auto var2)
                     {
                         auto *v1 = Vardata::get(scip, var1);
                         auto *v2 = Vardata::get(scip, var2);

                         assert(v1 != nullptr);
                         assert(v2 != nullptr);

                         return SCIP_Real(v1->profit()) / SCIP_Real(v1->weight()) >
                                SCIP_Real(v2->profit()) / SCIP_Real(v2->weight());
                     });

    return SCIP_OKAY;
}

SCIP_RETCODE Probdata::_release(SCIP *scip)
{
    for (auto &var : _vars)
    {
        SCIP_CALL(SCIPreleaseVar(scip, &var));
    }

    return SCIP_OKAY;
}

SCIP_RETCODE Probdata::scip_delorig(SCIP *scip)
{
    return _release(scip);
}

SCIP_RETCODE Probdata::scip_trans(SCIP *scip,
                                  scip::ObjProbData **objprobdata,
                                  SCIP_Bool *deleteobject)
{
    assert(objprobdata != nullptr);
    assert(deleteobject != nullptr);

    auto *t_probdata = new Probdata(*this);

    SCIP_CALL(SCIPtransformVars(scip,
                                std::size(t_probdata->_vars),
                                std::data(t_probdata->_vars),
                                std::data(t_probdata->_vars)));

    *objprobdata = t_probdata;
    *deleteobject = true;

    return SCIP_OKAY;
}

SCIP_RETCODE Probdata::scip_deltrans(SCIP *scip)
{
    return _release(scip);
}