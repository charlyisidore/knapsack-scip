/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "conshdlr.hpp"
#include "probdata.hpp"
#include "vardata.hpp"
#include <cassert>

const char *Conshdlr::Properties::Name = "kp";
const char *Conshdlr::Properties::Desc = "Constraint handler for the knapsack problem";
const int Conshdlr::Properties::Sepapriority = 0;
const int Conshdlr::Properties::Enfopriority = 0;
const int Conshdlr::Properties::Checkpriority = 0;
const int Conshdlr::Properties::Sepafreq = -1;
const int Conshdlr::Properties::Propfreq = -1;
const int Conshdlr::Properties::Eagerfreq = -1;
const int Conshdlr::Properties::Maxprerounds = 0;
const SCIP_Bool Conshdlr::Properties::Delaysepa = false;
const SCIP_Bool Conshdlr::Properties::Delayprop = false;
const SCIP_Bool Conshdlr::Properties::Needscons = false;
const SCIP_PROPTIMING Conshdlr::Properties::Proptiming = SCIP_PROPTIMING_BEFORELP;
const SCIP_PRESOLTIMING Conshdlr::Properties::Presoltiming = SCIP_PROPTIMING_ALWAYS;

Conshdlr::Conshdlr(SCIP *scip)
    : scip::ObjConshdlr(scip,
                        Properties::Name,
                        Properties::Desc,
                        Properties::Sepapriority,
                        Properties::Enfopriority,
                        Properties::Checkpriority,
                        Properties::Sepafreq,
                        Properties::Propfreq,
                        Properties::Eagerfreq,
                        Properties::Maxprerounds,
                        Properties::Delaysepa,
                        Properties::Delayprop,
                        Properties::Needscons,
                        Properties::Proptiming,
                        Properties::Presoltiming)
{
}

SCIP_RETCODE Conshdlr::scip_trans([[maybe_unused]] SCIP *scip,
                                  [[maybe_unused]] SCIP_CONSHDLR *conshdlr,
                                  [[maybe_unused]] SCIP_CONS *sourcecons,
                                  [[maybe_unused]] SCIP_CONS **targetcons)
{
    return SCIP_OKAY;
}

SCIP_RETCODE Conshdlr::scip_enfolp([[maybe_unused]] SCIP *scip,
                                   [[maybe_unused]] SCIP_CONSHDLR *conshdlr,
                                   [[maybe_unused]] SCIP_CONS **conss,
                                   [[maybe_unused]] int nconss,
                                   [[maybe_unused]] int nusefulconss,
                                   [[maybe_unused]] SCIP_Bool solinfeasible,
                                   SCIP_RESULT *result)
{
    *result = SCIP_FEASIBLE;
    return SCIP_OKAY;
}

SCIP_RETCODE Conshdlr::scip_enforelax([[maybe_unused]] SCIP *scip,
                                      [[maybe_unused]] SCIP_SOL *sol,
                                      [[maybe_unused]] SCIP_CONSHDLR *conshdlr,
                                      [[maybe_unused]] SCIP_CONS **conss,
                                      [[maybe_unused]] int nconss,
                                      [[maybe_unused]] int nusefulconss,
                                      [[maybe_unused]] SCIP_Bool solinfeasible,
                                      SCIP_RESULT *result)
{
    *result = SCIP_FEASIBLE;
    return SCIP_OKAY;
}

SCIP_RETCODE Conshdlr::scip_enfops([[maybe_unused]] SCIP *scip,
                                   [[maybe_unused]] SCIP_CONSHDLR *conshdlr,
                                   [[maybe_unused]] SCIP_CONS **conss,
                                   [[maybe_unused]] int nconss,
                                   [[maybe_unused]] int nusefulconss,
                                   [[maybe_unused]] SCIP_Bool solinfeasible,
                                   [[maybe_unused]] SCIP_Bool objinfeasible,
                                   SCIP_RESULT *result)
{
    *result = SCIP_FEASIBLE;
    return SCIP_OKAY;
}

SCIP_RETCODE Conshdlr::scip_check(SCIP *scip,
                                  [[maybe_unused]] SCIP_CONSHDLR *conshdlr,
                                  [[maybe_unused]] SCIP_CONS **conss,
                                  [[maybe_unused]] int nconss,
                                  SCIP_SOL *sol,
                                  [[maybe_unused]] SCIP_Bool checkintegrality,
                                  [[maybe_unused]] SCIP_Bool checklprows,
                                  [[maybe_unused]] SCIP_Bool printreason,
                                  [[maybe_unused]] SCIP_Bool completely,
                                  SCIP_RESULT *result)
{
    assert(result != nullptr);

    const auto *probdata = Probdata::get(scip);

    assert(probdata != nullptr);

    const auto &vars = probdata->vars();
    auto rc = probdata->capacity();

    *result = SCIP_FEASIBLE;

    for (auto var : vars)
    {
        auto val = SCIPgetSolVal(scip, sol, var);

        auto *vardata = Vardata::get(scip, var);
        auto w = val * vardata->weight();

        if (rc < w)
        {
            *result = SCIP_INFEASIBLE;

            if (printreason)
            {
                SCIPinfoMessage(scip,
                                nullptr,
                                "violation: knapsack constraint\n");
            }
            break;
        }

        rc -= w;
    }

    return SCIP_OKAY;
}

SCIP_RETCODE Conshdlr::scip_lock([[maybe_unused]] SCIP *scip,
                                 [[maybe_unused]] SCIP_CONSHDLR *conshdlr,
                                 [[maybe_unused]] SCIP_CONS *cons,
                                 [[maybe_unused]] SCIP_LOCKTYPE locktype,
                                 [[maybe_unused]] int nlockspos,
                                 [[maybe_unused]] int nlocksneg)
{
    return SCIP_OKAY;
}