/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "heur.hpp"
#include "probdata.hpp"
#include "vardata.hpp"
#include <vector>

const char *Heur::Properties::Name = "greedy";
const char *Heur::Properties::Desc = "Greedy heuristic for the knapsack problem";
const char Heur::Properties::Dispchar = SCIP_HEURDISPCHAR_TRIVIAL;
const int Heur::Properties::Priority = 1;
const int Heur::Properties::Freq = 1;
const int Heur::Properties::Freqofs = 0;
const int Heur::Properties::Maxdepth = 0;
const SCIP_HEURTIMING Heur::Properties::Timing = SCIP_HEURTIMING_BEFORENODE;
const SCIP_Bool Heur::Properties::Usesubscip = false;

Heur::Heur(SCIP *scip)
    : scip::ObjHeur(scip,
                    Properties::Name,
                    Properties::Desc,
                    Properties::Dispchar,
                    Properties::Priority,
                    Properties::Freq,
                    Properties::Freqofs,
                    Properties::Maxdepth,
                    Properties::Timing,
                    Properties::Usesubscip)
{
}

SCIP_RETCODE Heur::scip_exec(SCIP *scip,
                             [[maybe_unused]] SCIP_HEUR *heur,
                             [[maybe_unused]] SCIP_HEURTIMING heurtiming,
                             [[maybe_unused]] SCIP_Bool nodeinfeasible,
                             SCIP_RESULT *result)
{
    assert(result != nullptr);

    *result = SCIP_DIDNOTFIND;

    const auto *probdata = Probdata::get(scip);

    assert(probdata != nullptr);

    const auto &vars = probdata->vars();
    auto rc = probdata->capacity();

    // Selected items
    std::vector<SCIP_VAR *> s;

    // Sum of profits of selected items
    SCIP_Longint profit = 0;

    // Loop over variables fixed to 1
    for (auto var : vars)
    {
        // Is var not fixed to 1?
        if (!SCIPisGT(scip, SCIPvarGetLbLocal(var), 0.5))
        {
            continue;
        }

        auto *vardata = Vardata::get(scip, var);

        assert(vardata != nullptr);

        auto w = vardata->weight();

        if (rc < w)
        {
            SCIPdebugMsg(scip, "infeasible\n");
            return SCIP_OKAY;
        }

        s.push_back(var);
        profit += vardata->profit();
        rc -= w;
    }

    // Loop over unfixed variables
    for (auto var : vars)
    {
        // Is var fixed?
        if (SCIPisLT(scip, SCIPvarGetUbLocal(var), 0.5) ||
            SCIPisGT(scip, SCIPvarGetLbLocal(var), 0.5))
        {
            continue;
        }

        auto *vardata = Vardata::get(scip, var);

        assert(vardata != nullptr);

        auto w = vardata->weight();

        // Can the item be selected?
        if (rc < w)
        {
            continue;
        }

        // Select item
        s.push_back(var);
        profit += vardata->profit();
        rc -= w;
    }

    assert(rc >= 0);

    SCIP_SOL *sol = nullptr;

    SCIP_CALL(SCIPcreateSol(scip, &sol, nullptr));

    assert(sol != nullptr);

    for (auto var : s)
    {
        SCIP_CALL(SCIPsetSolVal(scip, sol, var, 1.));
    }

    SCIPdebugMsg(scip, "heuristic obj = %lld\n", profit);

    SCIP_Bool stored = false;

    SCIP_CALL(SCIPtrySolFree(scip, &sol, true, false, false, false, false, &stored));

    assert(stored != false);

    *result = SCIP_FOUNDSOL;

    return SCIP_OKAY;
}
